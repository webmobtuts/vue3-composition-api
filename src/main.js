import { createApp } from 'vue'
import {createRouter, createWebHistory} from 'vue-router';
import App from './App.vue'
import Posts from "./pages/Posts";
import PostsByTag from "./pages/PostsByTag";
import PostsByAuthor from "./pages/PostsByAuthor";

const routes = [
    { path: '/', component: Posts },
    { path: '/posts-by-tag/:tag', component: PostsByTag },
    { path: '/posts-by-author/:authorid/:authorname', component: PostsByAuthor },
]

const router = createRouter({
    history: createWebHistory(),
    routes
});

const app = createApp(App);
app.use(router);
app.mount('#app');