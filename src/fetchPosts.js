import json from "./assets/posts.json";

export function fetchPosts (tag = null, author = null) {
    let posts = json.data;

    if(tag) {
        posts = posts.filter(item => item.tags.includes(tag));
    }

    if(author) {
        posts = posts.filter(item => item.owner.id == author);
    }

    return posts;
}