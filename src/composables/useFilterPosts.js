// eslint-disable-next-line no-unused-vars
import {ref, reactive, computed} from "vue";

export default function useFilterPosts(posts, preparePosts) {
    function filter(filterData) {

        let temp = preparePosts();

        posts.value = temp;

        if(filterData.keyword) {
            posts.value = temp.filter(item => (new RegExp(filterData.keyword , 'i')).test(item.text));
        }

        if(filterData.user) {
            posts.value = temp.filter(item => item.owner.id == filterData.user);
        }

        if(filterData.publishdate) {
            posts.value = temp.filter(item => (new Date(item.publishDate)).toDateString() >= (new Date(filterData.publishdate)).toDateString());
        }
    }

    return {
        filter, filteredPosts: posts
    };
}