import {onMounted, ref} from "vue";
import {fetchPosts} from "../fetchPosts";

export default function usePosts(criteria = null) {
    let posts = ref([]);

    let preparePosts = () => {};

    if(criteria) {
        if(criteria.tag) {
            preparePosts = () => fetchPosts(criteria.tag);
        } else if (criteria.author) {
            preparePosts = () => fetchPosts(null, criteria.author);
        }
    } else {
        preparePosts = () => fetchPosts();
    }

    onMounted(() => posts.value = preparePosts());

    return {
        posts, preparePosts
    };
}